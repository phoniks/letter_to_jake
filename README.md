# letter_to_jake

Dear Jake,

Thanks for taking the time to chat with us on Friday during the AMA session.  I 
have a confession to make, which is that I didn't take full advantage of that 
opportunity. For me, there was a burning question that needed to be addressed 
regarding Amanda's absence. In the moment I feared being the only one who cared 
about the issue, and didn't want to be the 'debbie downer' who derailed a very 
positive conversation by raising it. So, regretably, I let the opportunity pass,
but in our celebrations later that evening it emerged that several of us shared 
similar concerns and had similar misgivings about the way that situation was 
handled. 

Rather than get into the weeds of recapping what happened, from my perspective, I
hope to keep this discussion at a philosophical level. To start, I'd like to
share with you a revelation that I had while at the Internet Identity Workshop
this week - one which probably wouldn't have occured without Amanda having been
a member of this community. The revelation I had was that decentralized consensus
is primarily about trust. I know, you're saying, "7 weeks in and you're just now
realizing that this is all about trust?" And frankly, that's a fair point, but I
think it highlights the fact that its possible to understand something 
intellectually without having the sort of visceral experience with it that makes
its value clear to you.

A gentleman gave a great talk at IIW about meta-network effects, and part of the
point he made was that decentralized consensus is essentially commoditizing trust.
And in fact I was able to draw a link between that concept and my own project at
Insight because of a connection that I made because of Amanda. On the first day 
a guy introduced himself to the group, saying that he was doing land governance 
on the blockchain. I made a mental note to myself to find him at some point 
during the conference and talk to him because Amanda and I and a few other Fellows
had discussed the potential for real estate on the blockchain and I thought she'd 
be interested.

During my conversation with him I was explaining to him about Insight and we got
to discussing why the program was called "decentralized consensus" rather than 
"blockchain".  I told him that it was odd for me too at first, but that actually 
my Key Transparency project had made it clear why "blockchain" doesn't capture 
the entirety of what is happening in this field. Key Transparency is a protocol 
that can be implemented by messaging service providers who provide end-to-end 
encryption. The idea is that by creating a blockchain-like data structure (where
later information contains the hash of earlier information) to act as a public key
registry and having that validated by multiple actors in the ecosystem the end 
user is less reliant on that messaging service to ensure that their communications
are secure. So ultimately Key Transparency is about coming to a consensus around
the true values of public keys in a decentralized fashion, but it is NOT a blockchain.
It enables us to trust more easily because we have faith in the process by which 
a conclusion is reached.

Which brings me back to the point of this letter. By reaching the decision about
Amanda's future at Insight without soliciting feedback from all of the Fellows
you have undermined my faith in the process by which decisions are made. Ironically,
the reason given for the action was the need to make all the Fellows feel safe. 
For me, the lack of transparency in the process has made me feel less safe. In 
decentralized consensus terms your action made it seem that if 3/20 Fellows conspire
they have the power to eject someone from Insight at will. To be clear, I want to 
state unequivicolly that I don't discount the experiences of those three Fellows 
at all. My concern is for the integrity of the process.

Specifically, "racism" was given as part of the reason for dismissing Amanda. I 
think its important to make a distinction between racism and bigotry, first of all.
Racism deals with an ideology and systematic structures, whereas bigotry deals 
with individual behavior that can extend from such an ideology. Conflating those
two concepts is dangerous, and this situation perfectly illustrates how so. Here
you have Insight's largely white staff making a decision that may have a material
impact on the career trajectory of a woman of color without affording her the 
opportunity to respond to the allegations against her in a meaningful forum. I 
would argue that THAT is an example of racist systemic structures at play. In a 
sense, Insight is externalizing the cost of actually addressing racism onto a 
person of color.

None of this is meant to diminish the experiences of those who had issues with
Amanda. I believe that their perspectives are valid, that the harms the experienced
were NOT imaginary, and that Amanda bears some of the fault in that. It is to say,
however, that no one is "protected" by a process in which we take superficial, 
secret, reactionary action rather than have the hard conversations that are 
necessary to address racism as a community. The only way to address racism in a 
meaningful, substantial way is to have difficult, uncomfortable conversations.
And I don't want you to think that I don't understand the conundrum that Insight
is in. Obviously if someone is creating a hostile environment you have a responsibility
to act - but, again, the process matters. HOW you reach a conclusion is arguably
more important than the conclusion you reach.

Had these issues arisen in week one or two I think the situation would be very
different, but by week 6 the threshold for expelling someone from the program has
to be significantly higher. Amanda completed 6/7ths of her obligation to Insight.
In my opinion that entitles her to more consideration than a cursory investigation
which fails to even consider her point of view. I do believe that there are some
acts that warrant immediate expulsion, but nothing about Amanda's behavior during
the 6 weeks I spent with her suggested that she was likely to do anything in that
vein. And so, we arrive at the current situation, in which my faith in and respect
for the process has been undermined because the result doesn't comport with the
evidence that I have. And now that I know other Fellows share my concern I think 
its important to ask Insight for a few things.

Firstly, perhaps its best that Amanda not come into the office considering how 
things have transpired, but I think its appropriate that she be given support in
her job search. Secondly, I believe that Amanda deserves the opportunity to know
about and address the complaints made against her. And third, I'd like to ask that
Insight develop a process for addressing these issues, at least for the DC program,
that is in line with the fundamental concept around which our industry is aligned
which is to say a process that is closer to a decentralized consensus.

To make that third pint more concrete, I think the appropriate way to handle this
situationwould have been to buy lunch for the cohort and have an open conversation
about whats happening during our lunch hour. Had that occured more perspectives 
could have shed light on the situation in such a way that would have reduced the
bias in the information on which Insight's staff made it's decision.  
Misunderstandings would have certainly have been revealed, and potentially cleared
up. And maybe the ultimate decision would have been the same but arriving at the
conclusion as a group would have given us a confidence in the process that is lacking now. 

And just to round out the discussion, I want to make it clear again that I 
understand that Insight's motivation is to limit its liability. But it's clear to
me that secrecy in this situation has not made the problem go away, and in fact
it even does a poor job of concealing information.  We're a cohort of 20 people - 
there's no such thing as a secret in a group this small. I strongly believe that
the more effective strategy to limit Insight's exposure is to provide a process 
that we Fellows can trust. Transparency can be your greatest asset in situations 
like these.  Let's work together as a community to make this situation right, and 
even more importantly, let's lay a foundation for future decentralized consensus
cohorts to have unequivocal faith that they will be treated fairly. 

Sincerely,





